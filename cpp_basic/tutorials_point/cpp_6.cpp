/*******************Array of pointers**************************/
#include<iostream>
using namespace std;
const int MAX=3;
int main()
{
    int arr[MAX]={10,20,30};
    int *ptr[MAX];//Array of pointer 

    for(int i=0;i<MAX;i++)
    {
        ptr[i]=&arr[i];
    }
    for(int i=0;i<MAX;i++)
    {
        cout<<"Address of arr["<<i<<"]:";//cout & cin library is from library iostream
        cout<<ptr+i;
        cout<<" Value of arr["<<i<<"]:";
        cout<<*ptr[i]<<endl;
        
    }
    return 0;
}
/************************output************************************************
Address of arr[0]:0x7ffc7b4a7620 Value of arr[0]:10
Address of arr[1]:0x7ffc7b4a7628 Value of arr[1]:20
Address of arr[2]:0x7ffc7b4a7630 Value of arr[2]:30

***********************output************************************************/
