/************************Array vs pointer************************/
/*
op:
---
cpp_5.cpp: In function ‘int main()’:
cpp_5.cpp:13:10: error: lvalue required as increment operand
a++;//Invalid as a is an array name & array name generates a pointer constant so a is a constant and its value can't be changed
^
*/
#include<iostream>
using namespace std;

int main()
{
    int a[3]={10,20,200};
    int *ptr;
    ptr=a;
    for(int i=0;i<2;i++)
    {
        cout<<"value of a["<<i<<"]:"<<*ptr<<endl;
        a++;//Invalid as a is an array name & array name generates a pointer constant so a is a constant and its value can't be changed
    }
    return 0;
}
