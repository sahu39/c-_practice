/*******************************pointer to entire array vs array of pointer*****************************/
#include<iostream>
using namespace std;

int main()
{
    int arr[3]={10,100,1000};
    int *ptr;      //integer pointer
    int (*ptr2)[3];//pointer to an array of 3 integer

    ptr=arr;         //ptr points to oth index of arr;
    ptr2=&arr;       //ptr2 points to entire array of 3 integers arr.
    
    cout<<"value of ptr:"<<ptr<<endl;
    cout<<"value of ptr2:"<<ptr2<<endl;
    ptr++;
    ptr2++;
    cout<<"value of ptr:"<<ptr<<endl;
    cout<<"value of ptr2:"<<ptr2<<endl;
     return 0;
}
/********************Output************************
 value of ptr:0x7fffbfcd5f10
 value of ptr2:0x7fffbfcd5f10
 value of ptr:0x7fffbfcd5f14
 value of ptr2:0x7fffbfcd5f1c
 *************************************************/
