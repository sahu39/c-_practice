/*******************pointer vs array********************************/
#include<iostream>
using namespace std;

int main()
{
    int a[]={10,100,200};
    int *ptr;
    ptr=a;
    for(int i=0;i<3;i++)
    {
        cout<<"Address of a["<<i<<"]:"<<ptr<<endl;
        cout<<"Value at a["<<i<<"]:"<<*ptr<<endl;
        ptr++;
    }

    return 0;
}
