/**************************passing pointer to a function*************************/
#include<iostream>
using namespace std;
double getaverage(double *,int);
int main()
{
    double a[4]={1000,23,277,34};
    double avg;
    avg=getaverage(a,4);
    cout<<"Average value is:"<<avg<<endl;
    return 0;
    
}
double getaverage(double *arr,int size)
{
    double avg=0;
    int i;
    double sum=0;
    for(i=0;i<size;i++)
    {
        sum=sum+(*(arr+i));
    }
    avg=sum/size;
    return avg;
}
/*****************output******************
 Average value is:333.5
*****************output******************/
