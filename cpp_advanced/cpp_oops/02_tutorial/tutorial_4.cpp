/*************Parameterized Constructor & this pointer****************************/
#include<iostream>

using namespace std;

class Employee{
    public:
        int id;
        string name;
        float salary;

        Employee(int i,string n,float s)
        {
            this->id = i;
            this->name = n;
            this->salary = s;
//            cout<<"Default Constructor Employee()\n"<<endl;
        }
        void display();

};
void Employee::display()
{
    cout<<"emp id:"<<id<<endl;
    cout<<"emp name:"<<name<<endl;
    cout<<"emp salary:"<<salary<<endl;
    cout<<"------------------------"<<endl;
}

int main()
{
    Employee e1(145,"Sunil",35833.33);
    Employee e2(190,"rakesh",34599.66);
    e1.display();
    e2.display();

    return 0;



}
