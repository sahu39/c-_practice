#include<iostream>

using namespace std;
class Shape{
    public:
        virtual void draw(){
        cout<<"drawing..."<<endl;
        }

};

class Rectangular:public Shape{
    public:
        void draw(){
        cout<<"drawing Rectangular..."<<endl;
        }

};

class Circle:public Shape{
    public:
        void draw(){
        cout<<"drawing Circle..."<<endl;
        }

};

int main()
{
    Shape *s;
    Shape sh;

    Rectangular arc;
    Circle cir;

    s = &sh;
    s->draw();

    s = &arc;
    s->draw();

    s = &cir;
    s->draw();


    return 0;
}
