/*******************************Destructor******************************************/
#include<iostream>

using namespace std;

class Employee{
    public:
        Employee()
        {
            cout<<"Default Constructor Employee()"<<endl;
        }
        ~Employee()
        {
            cout<<"Default Destructor ~Employee()"<<endl;
        }

};

int main()
{
    Employee e1;
    Employee e2;

    return 0;



}
