/*********************Usage of static keyword**************************/
#include <iostream>
using namespace std;

class account{
    public:
        int accno;
        string name;
        static int count;
        account(int acno,string na){
            this->accno =  acno;
            this->name = na;
            count++;
        }
        void display(){
            cout<<"accno:"<<accno<<endl;
            cout<<"name"<<name<<endl;
            cout<<"========================"<<endl;
        }
};
int account::count = 0;

int main(){
    account a1 = account(339,"sunil");
    account a2 = account(342,"jaga");
    account a3 = account(347,"Balia");
    a1.display();
    a2.display();
    a3.display();
    cout<<"Total objects are:"<<account::count<<endl;
    return 0;
}
