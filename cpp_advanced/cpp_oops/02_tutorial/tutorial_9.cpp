#include<iostream>
using namespace std;
class base{

    public:
        virtual void show() = 0;

};

class derived : public base{

    public:
        void show()
        {
            cout<<"Inside the Derived class"<<endl;
        }

};
int main()
{
    base *ptr;
    derived d;
    ptr = &d;
    ptr->show();
    return 0;
}
