//Single Inheritance
#include<iostream>
using namespace std;

class Account{

    public:
        float salary;// = 60000.16;

};
class Programmer:public Account{

    public:
        float bonus;// = 5000.33;
};


int main()
{
    Programmer P1;
    P1.salary = 60000;
    P1.bonus = 5000;
    cout<<"Salary:"<<P1.salary<<endl;
    cout<<"Bonus:"<<P1.bonus<<endl;
    return 0;
}
