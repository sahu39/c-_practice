/*************Friend class example********************************/
#include <iostream>
using namespace std;
class B;
class A{
    private:
        int x;
    public:
        void set_data(int i){
            x = i;
        }
        friend int min(A,B);
};

class B{
    private:
        int y;
    public:
        void set_data(int i){
            y = i;
        }
        friend int min(A,B);
};

int min(A a,B b){
    int minimum;
    minimum = (a.x<b.y)?a.x:b.y;
    return minimum;
}

int main(){
    A a;
    B b;
    a.set_data(10);
    b.set_data(20);
    cout<<"Smaller element is:"<<min(a,b)<<endl;
}

