/********Usage of static keyword*********************/
#include<iostream>

using namespace std;
class account{
    public:
        int accno;
        string name;
        static int count;
        account(int acc_no,string na)
        {
            this->accno = acc_no;
            this->name  = na;
            count++;
        }
        void display()
        {
            cout<<accno<<" "<<name<<endl;
        }

};
int account::count = 0;
int main()
{
    account a1(120,"Sunil");
    account a2(130,"daju");
    account a3(1200,"bani");

    a1.display();
    a2.display();
    a3.display();
    
    cout<<"Total No of employess created:"<<account::count<<endl;
    return 0;
}
