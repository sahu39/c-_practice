/*******Usage of static keyword**********/
#include <iostream>
using namespace std;

class Account{
    public:
        int Accno;
        string name;
        static float rateOfInterest;

        Account(int accno, string my_name){
            this->Accno = accno;
            this->name = my_name;
        }
        
        void display(){
            cout<<"Accno:"<<Accno<<endl;
            cout<<"name:"<<name<<endl;
            cout<<"rateOfInterest:"<<rateOfInterest<<endl;
        }
};

float Account::rateOfInterest = 6.5;

int main()
{
    Account a1 = Account(339,"Sunil");
    Account a2 = Account(340,"Swagat");
    a1.display();
    a2.display();
    return 0;
}
