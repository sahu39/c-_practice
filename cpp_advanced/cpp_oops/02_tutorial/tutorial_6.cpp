#include<iostream>

using namespace std;
class A{
 public:
     int a;
     void display()
     {
         cout<<"parent class"<<endl;
         cout<<a<<endl;
     }
};
class B:public A{
 public:
     int b;
     void display_b()
     {
         cout<<"child class"<<endl;
         cout<<b<<endl;
     }
};
int main()
{
    B b1;
    b1.a = 30;
    b1.b = 40;
    b1.display();
    b1.display_b();

    return 0;
}
