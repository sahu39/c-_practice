/****************Friend function***************/
#include <iostream>
using namespace std;

class Box{
    private:
        int length;
    public:
        Box(int l){
            this->length = l;
            //cout<<"Box Constructor"<<endl;
        }
        friend void display_length(Box);
};

void display_length(Box b){
    cout<<"Box length is:"<<b.length<<endl;
}

int main(){
    Box my_box = Box(10);
    display_length(my_box);
    return 0;
}
