//Accessing Data members
#include <iostream>
using namespace std;

class Box{
    public:
        double length;
        double breadth;
        double height;
    
};

int main(){
    Box box1;
    Box box2;
    double volume = 0.0;

    box1.length = 4.5;
    box1.breadth = 9.4;
    box1.height = 7.2;


    box2.length = 4.8;
    box2.breadth = 9.5;
    box2.height = 7.5;
    
    volume = box1.length * box1.breadth * box1.height;
    cout << "Voume of box1 is :"<<volume<<endl;
    
    volume = box2.length * box2.breadth * box2.height;
    cout << "Voume of box2 is :"<<volume<<endl;
    return 0;
}
