//C++ Class Member Functions
#include <iostream>
using namespace std;

class Box{
    public:
        double length;
        double breadth;
        double height;
        double getvolume(void);
        void setlength(double l);
        void setbreadth(double b);
        void setheight(double h);
};
void Box:: setlength(double l)
{
    length = l;
}
void Box:: setbreadth(double b)
{
    breadth = b;
}
void Box::setheight(double h)
{
    height = h;
}
double Box:: getvolume(void)
{
    return length*breadth*height;
}
int main(){
    Box box1;
    Box box2;
    double volume = 0.0;

    box1.setlength(4.5);
    box1.setbreadth(9.4);
    box1.setheight(7.2);


    box2.setlength(4.8);
    box2.setbreadth(9.5);
    box2.setheight(7.5);
    
    volume = box1.getvolume();
    cout << "Voume of box1 is :"<<volume<<endl;
    
    volume = box2.getvolume();
    cout << "Voume of box2 is :"<<volume<<endl;
    return 0;
}
