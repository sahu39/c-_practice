#include<iostream>

using namespace std;

class shape{
    public:
        void setwidth(int w)
        {
            width = w;
        }
        void setheight(int h)
        {
            height = h;
        }

    protected:
        int width;
        int height;

};

//Derived Class
class Rectangle:public shape{
    public:
        int getArea(){
            return (width*height);
        }
};

int main()
{
    Rectangle rect;
    rect.setwidth(5);
    rect.setheight(10);
    cout<<"Total Area:"<<rect.getArea()<<endl;
    return 0;
}
