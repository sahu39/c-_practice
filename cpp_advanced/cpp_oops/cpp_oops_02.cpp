#include <iostream>
using namespace std;

class Box{

    public:
        double length,breadth,height;
        double getvolume(void);
        double setlength(double len);
        double setbreadth(double bre);
        double setheight(double hei);
};

double Box :: setlength(double len)
{
    length = len;
}
double Box :: setbreadth(double bre)
{
    breadth = bre;
}
double Box :: setheight(double hei)
{    
    height = hei;
}
double Box :: getvolume(void)
{    
   return length*breadth*height;
}

int main()
{
    Box box1;
    box1.setlength(10);
    box1.setbreadth(11);
    box1.setheight(12);
    cout<<"volume of box1:"<<box1.getvolume()<<endl;
    return 0;
}


