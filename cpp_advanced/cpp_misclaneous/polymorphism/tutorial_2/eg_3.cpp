#include <iostream>
using namespace std;

class Weapon
{
        public:
                   virtual void Features()
                                { cout << "Loading weapon features.\n"; }
};

class Bomb : public Weapon
{
        public:
                   void Features()
                                { cout << "Loading bomb features.\n"; }
};

class Gun : public Weapon
{
        public:
                   void Features()
                                { cout << "Loading gun features.\n"; }
};

class loader
{
        public:
                   void loadFeatures(Weapon *w)
                                { w -> Features();}
};

int main()
{
    loader *l = new loader;
    Weapon *w;
    Bomb b;
    Gun g;

//    l->loadFeatures(w);

    w = &b;
    l->loadFeatures(w);

    w = &g;
    l->loadFeatures(w);
    return 0;
}
