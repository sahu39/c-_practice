#include<iostream>

using namespace std;

class shape{
    protected:
        float l;
    public:
        void getData()
        {
            cin >> l;
        }
        virtual float calculateArea() = 0;



};

class square : public shape{
    public:
        float calculateArea()
        {
            return l*l;
        }

};

class circle : public shape{
    public:
        float calculateArea()
        {
            return (3.14 * l * l);
        }


}


int main()
{
    square s;
    circle c;
}
