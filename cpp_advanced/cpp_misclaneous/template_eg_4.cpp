#include<iostream>

using namespace std;

template <class T>

class mycontainer{
    T ele;
    public:
    mycontainer(T arg)
    {
        ele = arg;
    }
    T increase()
    {
        return ++ele;
    }

};
template<>
class mycontainer <char>{
    char c;
    public:
    mycontainer(char arg)
    {
        c = arg;
    }
    char uppercase()
    {
        if((c >= 'a') && (c <= 'z'))
        {
            c += 'A' - 'a';
        }
        return c;
    }
};

int main()
{
    mycontainer<int>myint(8);
    mycontainer<char>mychar('c');
    cout<<myint.increase()<<endl;
    cout<<mychar.uppercase()<<endl;

    return 0;
}
