#include<iostream>
#include<thread>

//using namespace std;

void threadfun()
{
    for(int i = 0;i < 10000; i++);
    
    std::cout<<"Thread function Executing"<<std::endl;
    
}

int main()
{
    std::thread threadobj(threadfun);
    for(int i=0;i<10000;i++);

    std::cout<<"Display from main thread..."<<std::endl;
    threadobj.join();
    std::cout<<"Exiting from main thread..."<<std::endl;
    return 0;
}
