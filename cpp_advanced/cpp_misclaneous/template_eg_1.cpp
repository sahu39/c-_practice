#include<iostream>

using namespace std;

template <class myType>
myType GetMax(myType a,myType b)
{
    myType result;
    result = (a > b) ? a : b;
    return result;
}

int main()
{
    int a;
    double b;
    a = GetMax <int> (10,20);
    b = GetMax <double> (10.2,10.1);
    cout<<a<<endl;
    cout<<b<<endl;


    return 0;
}
